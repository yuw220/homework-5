1. 
(λp.pz) λq.w λw.wqzp
((λp.(p z)) (λq.(w (λw.(((w q) z) p)))))
((λx.(x z)) (λy.(w (λw.((((w y) z) x))))))

2. 
λp.pq λp.qp
(λp.((p q) (λp.(q p))))


## Question 2
inside " " is free
1. λs.s z λq.s q
(λs.((s "z") (λq.(s q))))

2. (λs. s z) λq. w λw. w q z s
(λs. (s "z")) (λq. ("w" (λw. (((w q) "z") "s"))))

3. (λs.s) (λq.qs)
(λs.s) (λq.(q "s"))

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
(λz. (((λs.s "q") (λq.q z))λz. (z z)))


## Question 3
1. 
(λz.z) (λq.q q) (λs.s a)           
-> (λq.q q) (λs.s a)               replace z with (λq.q q)
-> (λs.s a) (λs.s a)               replace q with (λs.s a)
-> (λs.s a) a                      replace s with (λs.s a) 
-> aa                              replace s with a

2. 
(λz.z) (λz.z z) (λz.z q)
-> (λz.z z) (λz.z q)               replace z with (λz.z z)
-> (λz.z q) (λz.z q)               replace z with (λz.z q)
-> (λz.z q) q                      replace z with (λz.z q)
-> qq                              replace z with p

3. 
(λs.λq.s q q) (λa.a) b
-> (λq.(λa.a) q q) (λa.a) b        replace s with (λa.a)
-> ((λa.a) b b)                    replace q with b
-> bb                              replace a with b

4. 
(λs.λq.s q q) (λq.q) q
-> (λs.λq'.s q' q') (λq.q) q''       rename q with q' and q''
-> (λq'.(λq.q) q'' q'') (λq.q) q''   replace s with (λq.q)
-> ((λq.q) q'' q'')                  replace q' with q
-> q'' q''                           replace q with q''
-> qq                                rename q'' back to q

5. 
((λs.s s) (λq.q)) (λq.q)
-> ((λq.q) (λq.q)) (λq.q)          replace s with (λq.q))
-> ((λq.q) (λq.q))                 replase q with (λq.q)
-> (λq.q)                          replase q with (λq.q)


## Question 4
1. 
p q or
T T T
T F T
F T T
F F F

2. The Church encoding for OR = (λp.λq.p p q)
T = (λx.λy.x)
F = (λx.λy.y)
OrTT -> T
(λp.λq.p p q)(λx.λy.x)(λx.λy.x)
-> (λq.(λx.λy.x)(λx.λy.x) q)(λx.λy.x)   replace p with (λx.λy.x)
-> ((λx.λy.x)(λx.λy.x)(λx.λy.x))        replace q with (λx.λy.x)
->((λy.(λx.λy.x))(λx.λy.x))             replace x with (λx.λy.x)
-> (λx.λy.x)                            replace y with (λx.λy.x)
-> T                                    definition of T

OrTF -> T
(λp.λq.p p q)TF
-> (λq.T T q)F            replace p with T
-> TTF                    replace q with F
-> (λx.λy.x)TF            replace T with definition
-> T                      return the first input

OrFT -> T
(λp.λq.p p q)FT
-> (λq.F F q)T            replace p with F
-> FFT                    replace q with T
-> (λx.λy.y)FT            replace F with definition
-> T                      return the second input

OrFF -> F
(λp.λq.p p q)FF
-> (λq.F F q)F            replace p with F
-> FFF                    replace q with F
-> (λx.λy.y)FF            replace F with definition
-> F                      return the second input


## Question 5
NOT = (λa.aFT)

NOT F -> T
(λa.aFT)F 
-> FFT                   replace a with F
-> (λx.λy.Y)FT           rewrite F with definition
-> T                     return the second input

it's similar to an IF statement becauce if the input is T then it returns one in the first place which is F and if the input is F then it returns the one in the second place T